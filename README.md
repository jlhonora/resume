Jose Luis Honorato - Resume
===========================
This repository contains the source code for [my resume (pdf)](https://bitbucket.org/jlhonora/resume/src). Feel free to fork it and modify it. You need to have LaTeX installed to build it.

To build the document go to the command line and type:

```
$ git clone https://jlhonora@bitbucket.org/jlhonora/resume.git
$ ./build
```

Based on [this work](http://github.com/mdwrigh2/resume).